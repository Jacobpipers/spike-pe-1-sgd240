# Spike Report

## SPIKE PE 1 - Collision and Trace

### Introduction

We want to understand the capabilities of the Physics Engine that Unreal uses. We should begin by looking at the basics of all physics engines – Collision and Raycasting!

### Goals

*	The Spike Report should answer each of the Gap questions

Starting with the FPS (blueprint) template, add the following features:

*	Change the weapon so that it fires “hitscan” style (i.e. instant hit, no projection)
	*	You will need to use Traces (raycasts)
	*	Ensure that the trace comes from the gun barrel, and not the player location/etc.

*	You may need to use a Socket
	*	It should play a Sound on impact, at the impact location
	*	It should apply a Decal or Particle effect to any surface struck.

*	Create a blueprint which is of a translucent box, which does not block the weapon trace, but does block the player
	*	i.e. you can’t move through it, but you can shoot through it
	*	to create the translucent material, ask a Graphics Programming specialist for assistance

*	Use Physics Channels and Collision Responses to create interesting scenarios. You must implement/use each of the following. The Collision documentation will prove helpful.
	*	Hit Events
	*	Different Object Types
	*	Overlap Events // Play Around.
	*	Objects which Hit or Overlap different Object Types



### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* Visual Studio
* Unreal Engine
* [Physics Documentation](https://docs.unrealengine.com/latest/INT/Engine/Physics/)
* [Single Line Trace Documentation](https://docs.unrealengine.com/latest/INT/Engine/Physics/Tracing/HowTo/SingleLineTraceByObject/index.html)
* [Traces Overview](https://docs.unrealengine.com/latest/INT/Engine/Physics/Tracing/Overview/index.html)
* [Line Trace YouTube](https://www.youtube.com/watch?v=0i5yKDphSrA)


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Create New Project Ussing FPS (blueprint) Template - Include Starter Content
2. Open First Personn Character Blueprint.
	* Create a function called laser
	* As seen in the Line trace documentation Use LineTraceByChannel
		* Set the Line Trace Channel to Visible 
		* Get Start Location 
		* Get End Location -Distance of Trace
	* Apply [Decal](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Rendering/Components/Decal/SpawnDecalatLocation/index.html), [Sound](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Audio/PlaySoundatLocation/index.html) and [Emitter](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Effects/Components/ParticleSystem/SpawnEmitteratLocation/index.html) at impact location
		* Decal pass in Impact Normal
3. Create Transluecent box
	* Create a new Blueprint class call it transluecentBox
	* Add A static mesh, use Asset: Wall_400_300 from Starter Content
	* Apply M_Glass Asset to Meterial slot of static mesh - The Materials Blend mode should be set to Translucent
	* Add A Box Collision Component to Blueprint. Set as root.
	* Set Collision Preset to Ignore [vissible Channel](https://docs.unrealengine.com/latest/INT/Engine/Physics/Collision/Overview/).
		* This Allows any physics channel set as vissible to pass through. This allows the Laser to go through the glass or transluecent objects you want it to pass through, but still lets the player to collides with the visible objects.



### What we found out

Unreal uses the Nvidia PhysX Engine, As well as Box2D for 2d Physics.
	
Knowledge: What capabilities does Unreal expose to us?

Exposes the Use of Collisions, Constraints, raycast with the use of Traces. It also exposes physic bodies, physics materials in use of the engine see [this](https://docs.unrealengine.com/latest/INT/Engine/Physics/) to lern more about the physics in unreal.

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.